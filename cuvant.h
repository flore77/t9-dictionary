/* Simion Florentin 311CA */

#ifndef CUVANT_H_INCLUDED
#define CUVANT_H_INCLUDED

using namespace std;

class Cuvant {
    int aparitii;
    string text;
public:
    Cuvant () {};

    Cuvant (string cuvant, int aparitii) {
        this->text = cuvant;
        this->aparitii = aparitii;
    }
    int operator> (Cuvant cuv) {
        if (this->aparitii == cuv.aparitii)
            return this->text < cuv.text;
        else
            return this->aparitii > cuv.aparitii;
    }
    int operator< (Cuvant cuv) {
        if (this->aparitii == cuv.aparitii)
            return this->text > cuv.text;
        else
            return this->aparitii < cuv.aparitii;
    }
    void operator++ (int) {
        this->aparitii = this->aparitii + 1;
    }
    friend ostream& operator<< (ostream &, const Cuvant &);
};

ostream& operator<< (ostream &out, const Cuvant &cuv) {
        out << cuv.text;
        return out;
    }

#endif // CUVANT_H_INCLUDED
