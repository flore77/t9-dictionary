/* Simion Florentin 311CA */

#ifndef TRIE_H_INCLUDED
#define TRIE_H_INCLUDED

#define fiu (*s - '2')
#include "treap.h"
#include <cstring>
using namespace std;

template<typename T>
class Trie {
    Treap<T> *treap;
    Trie *cifre[8];
public:

    Trie () {
        int i;
        for (i = 0; i < 8; i++)
            cifre[i] = NULL;
        treap = new Treap<T>;
    }

    void insert (const char *s, T value, int priority) {
        if (*s == '\n' || *s == '\0') {
            treap->insert(treap,value,priority);
            return;
        }
        if (cifre[fiu] == NULL) {
            cifre[fiu] = new Trie<T>;
        }
        cifre[fiu]->insert(s + 1,value,priority);
    }

    T interogare (const char *s, int k, int priority) {
        if (*s == '\n' || *s == '\0') {
            k = k % treap->dim();
            k = k + 1;
            T elem = treap->findK(k);
            treap->erase(treap,elem);
            elem++;
            treap->insert(treap,elem,priority);
            //cout << elem << "\n";
            return elem;
        }
        if (cifre[fiu] != NULL)
            return cifre[fiu]->interogare(s + 1, k, priority);
        else {
            cout << "Error - seria de taste nu exista in trie!" << s << endl;
            return T();
        }
    }

    ~Trie () {
        int i;
        for (i = 0; i < 8; i++) {
            if (cifre[i] != NULL)
                delete cifre[i];
        }
        delete treap;
    }
};

#endif // TRIE_H_INCLUDED
