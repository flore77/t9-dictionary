/* Simion Florentin 311CA */

#ifndef TRIE_H_INCLUDED
#define TRIE_H_INCLUDED

using namespace std;

template <typename T>
struct Treap {
    Treap<T> *left, *right;
    T key;
    int priority;
    bool nil;
    int nr_nodes;

    Treap () {
        nil = true;
        nr_nodes = 0;
        priority = -1;
        left = right = NULL;
    }

    void addData (T key, int priority) {
        this->nil = false;
        this->key = key;
        this->priority = priority;
        this->nr_nodes = 1;
        this->left = new Treap();
        this->right = new Treap();
    }


      void delData () {
        this->nil = true;
        this->priority = -1;
        delete this->left;
        delete this->right;
        this->nr_nodes = 0;
      }

      bool find(T key) {
        if (this->isNil()) {
          return false;
        }

        if (key == this->key) {
            return true;
        }

        if (key < this->key) {
            return left->find(key);
        } else {
            return right->find(key);
        }

    }

      void rotateRight(Treap<T> *&nod) {
        Treap<T> *aux;

        aux = nod->left;

        nod->nr_nodes = nod->nr_nodes - nod->left->nr_nodes + aux->right->nr_nodes;
        aux->nr_nodes += nod->right->nr_nodes + 1;

        nod->left = aux->right;
        aux->right = nod;
        nod = aux;

      }

      void rotateLeft(Treap<T> *&nod) {

        Treap<T> *aux;

        aux = nod->right;

        nod->nr_nodes = nod->nr_nodes - nod->right->nr_nodes + aux->left->nr_nodes;
        aux->nr_nodes += nod->left->nr_nodes + 1;

        nod->right = aux->left;
        aux->left = nod;
        nod = aux;

      }

  void insert(Treap<T> *&fatherPointer, T key, int priority) {
	this->nr_nodes++;

    if (this->isNil()) {
      this->addData(key, priority);

      return ;
    }

    if (this->key > key ) {
		fatherPointer->left->insert(fatherPointer->left, key, priority);
    } else {
		fatherPointer->right->insert(fatherPointer->right, key, priority);
    }

    if (this->left->priority > this->priority) {
		fatherPointer->rotateRight(fatherPointer);
    } else if (this->right->priority > this->priority) {
		fatherPointer->rotateLeft(fatherPointer);
    }
  }

  void erase(Treap<T> *&fatherPointer, T key) {
    if (this->isNil()) {
      return ;
    }

	this->nr_nodes--;

    if (this->key > key) {
		fatherPointer->left->erase(fatherPointer->left, key);
    } else if (this->key < key) {
		fatherPointer->right->erase(fatherPointer->right, key);
    } else if (this->left->isNil() && this->right->isNil()) {
      this->delData();
    } else {
      if (this->left->priority > this->right->priority) {
			fatherPointer->rotateRight(fatherPointer);
			fatherPointer->erase(fatherPointer, key);
      } else {
			fatherPointer->rotateLeft(fatherPointer);
			fatherPointer->erase(fatherPointer, key);
      }
    }
  }

  int dim () {
    return this->nr_nodes;
  }

        T findMax () {
        if (nil == true)
            return T();
        if (right->nil != true)
            return right->findMax();
        else
            return key;
    }

  T findK(int k) {

	if (k == this->right->nr_nodes + 1) {
		return this->key;
	}

	if (k < this->right->nr_nodes + 1) {
		return this->right->findK(k);
	} else {
		k = k - this->right->nr_nodes - 1;
		return this->left->findK(k);
	}

  }

};

#endif
