/* Simion Florentin 311CA */

#include <iostream>
#include "trie.h"
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "cuvant.h"
using namespace std;

int main() {
    ifstream in;
    in.open("date.in");
    ofstream out;
    out.open("date.out");
    Trie<Cuvant> *trie = new Trie<Cuvant>;
    int n, m, nr_aparitii, size,i ,j;
    string cuv, cifre;
    srand(time(NULL));
    in >> n;
    for (i = 0; i < n; i++) {
        in >> cuv;
        in >> nr_aparitii;
        Cuvant elem(cuv,nr_aparitii);
        size = cuv.length();
        for (j = 0; j < size; j++) {
            switch (cuv[j]) {
                case 'a': cifre[j] = '2'; continue;
                case 'b': cifre[j] = '2'; continue;
                case 'c': cifre[j] = '2'; continue;
                case 'd': cifre[j] = '3'; continue;
                case 'e': cifre[j] = '3'; continue;
                case 'f': cifre[j] = '3'; continue;
                case 'g': cifre[j] = '4'; continue;
                case 'h': cifre[j] = '4'; continue;
                case 'i': cifre[j] = '4'; continue;
                case 'j': cifre[j] = '5'; continue;
                case 'k': cifre[j] = '5'; continue;
                case 'l': cifre[j] = '5'; continue;
                case 'm': cifre[j] = '6'; continue;
                case 'n': cifre[j] = '6'; continue;
                case 'o': cifre[j] = '6'; continue;
                case 'p': cifre[j] = '7'; continue;
                case 'q': cifre[j] = '7'; continue;
                case 'r': cifre[j] = '7'; continue;
                case 's': cifre[j] = '7'; continue;
                case 't': cifre[j] = '8'; continue;
                case 'u': cifre[j] = '8'; continue;
                case 'v': cifre[j] = '8'; continue;
                case 'w': cifre[j] = '9'; continue;
                case 'x': cifre[j] = '9'; continue;
                case 'y': cifre[j] = '9'; continue;
                case 'z': cifre[j] = '9'; continue;
            }
        }
        cifre[j] = '\0';
        trie->insert(cifre.c_str(),elem,rand());
    }
    int k;
    in >> m;
    for (i = 0; i < m; i++) {
        in >> cifre;
        size_t found = cifre.find('*');
        if (found != string::npos) {
			string nr(cifre, found + 1, string::npos);

			k = atoi(nr.c_str());

			cifre.erase(found, string::npos);
		}
        else
            k = 0;
        Cuvant elem = trie->interogare(cifre.c_str(),k,rand());
        if (j == m - 1)
            out << elem;
        else
            out << elem << "\n";
    }
    delete trie;
    return 0;
}
